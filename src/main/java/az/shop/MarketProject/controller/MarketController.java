package az.shop.MarketProject.controller;

import az.shop.MarketProject.dto.MarketRequest;
import az.shop.MarketProject.dto.MarketResponse;
import az.shop.MarketProject.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @PostMapping("/{marketId}/regions/{regionId}")
    public MarketResponse assignRegionToMarket(@RequestBody MarketRequest request,
                                               @PathVariable long marketId,
                                               @PathVariable long regionId) {
        // Call the service method to assign a region to a market
        MarketResponse response = marketService.assignRegionToMarket(request, marketId, regionId);
        return response;
    }

    @PostMapping
    public MarketResponse create(@RequestBody @Valid MarketRequest request) {
        return marketService.create(request);
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId) {
        marketService.delete(marketId);
    }

    /*@PutMapping("/{marketId}")
    public MarketResponse updateMarket(@PathVariable Long marketId, @RequestBody MarketRequest marketRequest) {
        MarketResponse response = marketService.updateMarket(marketId, marketRequest);
        return response;
    }*/

    @PutMapping("/{marketId}")
    public MarketResponse updateMarket(
            @PathVariable Long marketId,
            @RequestBody MarketRequest marketRequest,
            @RequestParam Long regionId
    ) {
        return marketService.updateMarket(marketId, marketRequest, regionId);
    }

    @GetMapping("/{marketId}")
    public MarketResponse getMarketById(@PathVariable Long marketId) {
        MarketResponse response = marketService.getMarketById(marketId);
        return response;
    }

    @GetMapping("/list")
    public List<MarketResponse> getAllMarketsById(@RequestParam List<Long> marketIds) {
        List<MarketResponse> marketResponses = marketService.getAllMarketsById(marketIds);
        return marketResponses;
    }

}

package az.shop.MarketProject.controller;


import az.shop.MarketProject.dto.AddressRequest;
import az.shop.MarketProject.dto.AddressResponse;
import az.shop.MarketProject.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest request) {
        return addressService.create(branchId, request);
    }
}

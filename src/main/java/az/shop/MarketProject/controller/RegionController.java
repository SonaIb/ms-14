package az.shop.MarketProject.controller;

import az.shop.MarketProject.dto.RegionRequest;
import az.shop.MarketProject.dto.RegionResponse;
import az.shop.MarketProject.model.Region;
import az.shop.MarketProject.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {

    private final ModelMapper modelMapper;
    private final RegionService regionService;

    @GetMapping("/regions/{ids}")
    public List<RegionResponse> getRegionsById(@PathVariable List<Long> ids) {
        return regionService.findAllRegionById(ids);
    }

    @GetMapping("/{regionId}")
    public RegionResponse findRegionById(@PathVariable Long regionId) {
        return regionService.findRegionById(regionId);
    }

    @PostMapping("/regions")
    public RegionResponse create(RegionRequest request) {
        RegionResponse response = regionService.create(request);
        return response;
    }

    @PostMapping("/{id}")
    public RegionResponse update(@PathVariable Long id, @RequestBody RegionRequest request) {
        RegionResponse response = regionService.update(id, request);
        return response;
    }


    @DeleteMapping("/{regionId}")
    public void deleteRegion(@PathVariable Long regionId) {
        regionService.delete(regionId);
    }

}

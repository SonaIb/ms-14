package az.shop.MarketProject.service;


import az.shop.MarketProject.dto.AddressRequest;
import az.shop.MarketProject.dto.AddressResponse;
import az.shop.MarketProject.model.Address;
import az.shop.MarketProject.model.Branch;
import az.shop.MarketProject.repository.AddressRepository;
import az.shop.MarketProject.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;

    public AddressResponse create(Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);
    }

    public void deleteAddress(Address adress){
        addressRepository.delete(adress);
    }

    public AddressResponse update(@PathVariable Long branchId, @PathVariable Long addressId, AddressRequest request){

        Optional<Branch> branch = branchRepository.findById(branchId);

        return null;
    }




















}

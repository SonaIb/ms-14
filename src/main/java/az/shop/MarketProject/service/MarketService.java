package az.shop.MarketProject.service;

import az.shop.MarketProject.dto.MarketRequest;
import az.shop.MarketProject.dto.MarketResponse;
import az.shop.MarketProject.model.Market;
import az.shop.MarketProject.model.Region;
import az.shop.MarketProject.repository.MarketRepository;
import az.shop.MarketProject.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import jakarta.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarketService {
    private final MarketRepository marketRepository;
    private final RegionRepository regionRepository;
    private final ModelMapper modelMapper;


    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    public MarketResponse assignRegionToMarket(MarketRequest request, long marketId, long regionId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new EntityNotFoundException("Market not found"));
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new EntityNotFoundException("Region not found"));
        List<Region> regions = new ArrayList<>(market.getRegions());
        regions.add(region);
        market.setRegions(regions);
        marketRepository.save(market);
        ModelMapper modelMapper = new ModelMapper();
        MarketResponse response = modelMapper.map(market, MarketResponse.class);
        return response;
    }
    public void delete(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Market with id %d not found", marketId)));
        marketRepository.delete(market);
    }

    public MarketResponse updateMarket(Long marketId, MarketRequest marketRequest, Long regionId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Market with id %d not found", marketId)));
        modelMapper.map(marketRequest, market);
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Region with id %d not found", regionId)));
        List<Region> regions = new ArrayList<>();
        regions.add(region);
        market.setRegions(regions);
        Market savedMarket = marketRepository.save(market);
        return modelMapper.map(savedMarket, MarketResponse.class);
    }

    public MarketResponse getMarketById(Long id) {
        Market market = marketRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Market with id %d not found", id)));
        return modelMapper.map(market, MarketResponse.class);
    }
    public List<MarketResponse> getAllMarketsById(List<Long> marketIds) {
        List<Market> markets = marketRepository.findAllById(marketIds);
        if (markets == null || markets.isEmpty()) {
            throw new RuntimeException("No markets found with the provided IDs");
        }
        return markets.stream()
                .map(market -> modelMapper.map(market, MarketResponse.class))
                .collect(Collectors.toList());
    }

}

package az.shop.MarketProject.service;


import az.shop.MarketProject.dto.BranchRequest;
import az.shop.MarketProject.dto.BranchResponse;
import az.shop.MarketProject.model.Branch;
import az.shop.MarketProject.model.Market;
import az.shop.MarketProject.repository.BranchRepository;
import az.shop.MarketProject.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;


    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        return null;
    }
}

package az.shop.MarketProject.service;
import az.shop.MarketProject.dto.RegionRequest;
import az.shop.MarketProject.dto.RegionResponse;
import az.shop.MarketProject.model.Region;
import az.shop.MarketProject.repository.MarketRepository;
import az.shop.MarketProject.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import jakarta.persistence.EntityNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RegionService{

    private final ModelMapper modelMapper;
    private final RegionRepository regionRepository;

    public RegionResponse create(RegionRequest request) {
        Region region = modelMapper.map(request, Region.class);
        Region savedRegion = regionRepository.save(region);
        return modelMapper.map(savedRegion, RegionResponse.class);
    }

    public void delete(Long regionId) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Region with id %d not found", regionId)));
        regionRepository.delete(region);
    }


    public List<RegionResponse> findAllRegionById(List<Long> regionIds) {
        List<Region> regions = regionRepository.findAllById(regionIds);
        if (regions == null || regions.isEmpty()) {
            throw new RuntimeException("No regions found with the provided IDs");
        }
        return regions.stream()
                .map(region -> modelMapper.map(region, RegionResponse.class))
                .collect(Collectors.toList());
    }

    public RegionResponse findRegionById(Long id) {
        Region region = regionRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Region not found with provided ID"));
        return modelMapper.map(region, RegionResponse.class);
    }

    public RegionResponse update(Long id, RegionRequest request) {
        Region region = regionRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Region not found with provided ID"));
        modelMapper.map(request, region);
        Region updatedRegion = regionRepository.save(region);
        return modelMapper.map(updatedRegion, RegionResponse.class);
    }

}
package az.shop.MarketProject.dto;

import az.shop.MarketProject.model.Market;
import az.shop.MarketProject.model.RegionType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegionResponse {
    private Long id;
    @Enumerated(EnumType.STRING)
    RegionType name;


}

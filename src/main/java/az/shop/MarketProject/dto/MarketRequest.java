package az.shop.MarketProject.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketRequest {

    Long Id;
    @NotEmpty(message = "Can not be null or empty")
    String name;
    String type;
}

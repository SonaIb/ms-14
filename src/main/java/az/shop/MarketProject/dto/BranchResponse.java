package az.shop.MarketProject.dto;

import az.shop.MarketProject.model.Address;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchResponse {

    Long id;
    String name;
    Integer countOfEmployee;

    Address address;
}

package az.shop.MarketProject.repository;


import az.shop.MarketProject.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region, Long> {

}

package az.shop.MarketProject.repository;


import az.shop.MarketProject.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch, Long> {
}

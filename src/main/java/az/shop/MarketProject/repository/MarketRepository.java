package az.shop.MarketProject.repository;

import az.shop.MarketProject.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketRepository extends JpaRepository<Market, Long> {


}

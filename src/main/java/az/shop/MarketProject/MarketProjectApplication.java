package az.shop.MarketProject;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MarketProjectApplication {

	public static void main(String[] args) {

		SpringApplication.run(MarketProjectApplication.class, args);

	}

}
